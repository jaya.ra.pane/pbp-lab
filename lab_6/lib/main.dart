import 'package:flutter/material.dart';

//source :https://github.com/rajayogan/flutter-minimalloginUI/blob/master/lib/signup.dart
//https://youtu.be/2rn3XbBijy4
//https://youtu.be/ULSPXpVoogg
void main() {
  runApp(new MaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: const MyHomePage(),
  ));
}

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Form Vaksinasi',
//       theme: ThemeData(
//
//         primarySwatch: Colors.blue,
//       ),
//       // home: const MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  final _nama = TextEditingController();
  final _tanggalLahir = TextEditingController();
  final _nik = TextEditingController();
  final _vaksinKe = TextEditingController();
  final _tanggalVaksin = TextEditingController();
  final _lokasiVaksin = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new Icon(Icons.list),
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Daftar Vaksin"),
        backgroundColor: Colors.blue,
      ),
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              new Container(
                padding: new EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    new Text("Form Pendaftaran Vaksin",
                        style: new TextStyle(fontSize: 20.0)),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText: "Masukkan Nama Lengkap Anda",
                          labelText: "Nama Lengkap",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: _nama,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText: "Masukkan Tanggal Lahir Anda",
                          labelText: "Tanggal",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Tanggal Lahir tidak Boleh Kosong';
                        }
                        return null;
                      },
                      controller: _tanggalLahir,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText: "Masukkan NIK Anda",
                          labelText: "NIK",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'NIK tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: _nik,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText: "Vaksin 1 atau 2",
                          labelText: "Pendaftaran Vaksinasi Ke-",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Jumlah Vaksinasi tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: _vaksinKe,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                        decoration: new InputDecoration(
                            hintText:
                                "Masukkan Tanggal Vaksinasi yang Anda Inginkan",
                            labelText: "Tanggal Vaksin",
                            border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Tanggal tidak boleh kosong';
                          }
                          return null;
                        },
                        controller: _tanggalVaksin),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText:
                              "Masukkan Lokasi Vaksinasi Terdekat dari Lokasi Anda",
                          labelText: "Lokasi Vaksinasi",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Lokasi Vaksinasi tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: _lokasiVaksin,
                    ),
                    new Padding(padding: new EdgeInsets.only(right: 200.0)),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: new ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green, // background
                            onPrimary: Colors.white, // foreground
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {}
                          },
                          child: Text("Submit")),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

// body: Center(
//   // Center is a layout widget. It takes a single child and positions it
//   // in the middle of the parent.
//   child: Column(
//
//     mainAxisAlignment: MainAxisAlignment.center,
//     children: <Widget>[
//       const Text(
//         'You have pushed the button this many times:',
//       ),
//       Text(
//         '$_counter',
//         style: Theme.of(context).textTheme.headline4,
//       ),
//     ],
//   ),
// ),
// floatingActionButton: FloatingActionButton(
//   onPressed: _incrementCounter,
//   tooltip: 'Increment',
//   child: const Icon(Icons.add),
// ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
