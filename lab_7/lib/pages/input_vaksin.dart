import 'package:flutter/material.dart';
import './vaksin.dart';

class InputVaksin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputVaksinState();
  }
}


class InputVaksinState extends State<InputVaksin> {
  String _nama = "Joya Ruth Amanda";
  String _tanggalLahir = "23 Desember 2001";
  String _nik = "123456789";
  String _vaksinKe = "2";
  String _tanggalVaksin = "25 Desember 2021";
  String _lokasiVaksin = "Jakarta";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _buildNama() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Nama'),
      maxLength: 20,
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Nama!';
        }
      },
      onSaved: (value) {
        _nama = value.toString();
      },
    );
  }

  _buildTanggalLahir() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal Lahir'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Tanggal Lahir!';
        }
      },
      onSaved: (value) {
        _tanggalLahir = value.toString();
      },
    );
  }

  _buildNik() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'NIK'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan NIK!';
        }
      },
      onSaved: (value) {
        _nik = value.toString();
      },
    );
  }

  _buildVaksinKe() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Vaksin ke'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Vaksin ke berapa!';
        }
      },
      onSaved: (value) {
        _vaksinKe = value.toString();
      },
    );
  }

  _buildTanggalVaksin() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal Vaksin'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Tanggal Vaksin!';
        }
      },
      onSaved: (value) {
        _tanggalVaksin = value.toString();
      },
    );
  }
  _buildLokasiVaksin() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Lokasi Vaksin'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Lokasi Vaksin!';
        }
      },
      onSaved: (value) {
        _lokasiVaksin = value.toString();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Input Data Vaksin"),
        ),
        body: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildNama(),
                _buildTanggalLahir(),
                _buildNik(),
                _buildVaksinKe(),
                _buildTanggalVaksin(),
                _buildLokasiVaksin(),
                SizedBox(height: 100),
                RaisedButton(
                  child: Text('Submit',
                      style: TextStyle(color: Colors.blueAccent, fontSize: 16)),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }
                    _formKey.currentState!.save();
                    data.add({
                      "nama": _nama,
                      "tanggalLahir": _tanggalLahir,
                      "nik": _tanggalLahir,
                      "vaksinKe": _vaksinKe,
                      "tanggalVaksin": _tanggalVaksin,
                      "lokasiVaksin": _lokasiVaksin,
                    });
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Vaksin()));
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
