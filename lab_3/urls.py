from django.urls import path
from .views import add_friend, index



urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='addfriend')
    # path('xml', xml, name= 'xml'),
    # path('json', json, name= 'json'),
 
]